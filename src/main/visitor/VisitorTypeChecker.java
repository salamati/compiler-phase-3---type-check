package main.visitor;

import main.ast.node.Main;
import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.ActorInstantiation;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.declaration.handler.InitHandlerDeclaration;
import main.ast.node.declaration.handler.MsgHandlerDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.operators.BinaryOperator;
import main.ast.node.expression.operators.UnaryOperator;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;
import main.ast.type.actorType.ActorType;
import main.ast.type.arrayType.ArrayType;
import main.ast.type.noType.NoType;
import main.ast.type.primitiveType.BooleanType;
import main.ast.type.primitiveType.IntType;
import main.ast.type.primitiveType.StringType;
import main.symbolTable.SymbolTable;
import main.symbolTable.SymbolTableActorItem;
import main.symbolTable.SymbolTableHandlerItem;
import main.symbolTable.SymbolTableMainItem;
import main.symbolTable.itemException.ItemNotFoundException;
import main.symbolTable.symbolTableVariableItem.SymbolTableActorVariableItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableLocalVariableItem;
import main.symbolTable.symbolTableVariableItem.SymbolTableVariableItem;

import java.util.ArrayList;
import java.util.Objects;

public class VisitorTypeChecker implements InterfaceTypeChecker {
    private SymbolTableActorItem curr_act;
    private SymbolTableHandlerItem currHandler;
    private int forFlag = 0;
    private boolean initFlag = false;
    private boolean mainFlag = false;

    enum Primitives {
        INT, STRING, BOOLEAN, ARRAY
    }

    private boolean checkType(Type inputType, Primitives p){
        boolean res = false;
        switch (p) {
            case INT:
                res = inputType instanceof IntType;
                break;

            case STRING:
                res = inputType instanceof StringType;
                break;

            case ARRAY:
                res = inputType instanceof ArrayType;
                break;

            case BOOLEAN:
                res = inputType instanceof BooleanType;
                break;
        }
        return (res || inputType instanceof NoType);
    }



    protected void visitStatement( Statement stat )
    {
        if( stat == null )
            return;
        else if( stat instanceof MsgHandlerCall )
            this.visit( ( MsgHandlerCall ) stat );
        else if( stat instanceof Block )
            this.visit( ( Block ) stat );
        else if( stat instanceof Conditional )
            this.visit( ( Conditional ) stat );
        else if( stat instanceof For )
            this.visit( ( For ) stat );
        else if( stat instanceof Break )
            this.visit( ( Break ) stat );
        else if( stat instanceof Continue )
            this.visit( ( Continue ) stat );
        else if( stat instanceof Print )
            this.visit( ( Print ) stat );
        else if( stat instanceof Assign )
            this.visit((Assign) stat);
    }

    protected Type visitExpr( Expression expr )
    {
        if( expr == null )
            return null;
        else if( expr instanceof UnaryExpression )
            return this.visit( ( UnaryExpression ) expr );
        else if( expr instanceof BinaryExpression )
            return this.visit( ( BinaryExpression ) expr );
        else if( expr instanceof ArrayCall )
            return this.visit( ( ArrayCall ) expr );
        else if( expr instanceof ActorVarAccess )
            return this.visit( ( ActorVarAccess ) expr );
        else if( expr instanceof Identifier )
            return this.visit( ( Identifier ) expr );
        else if( expr instanceof Self )
            return this.visit( ( Self ) expr );
        else if( expr instanceof Sender )
            return this.visit( ( Sender ) expr );
        else if( expr instanceof BooleanValue )
            return this.visit( ( BooleanValue ) expr );
        else if( expr instanceof IntValue )
            return this.visit( ( IntValue ) expr );
        else if( expr instanceof StringValue )
            return this.visit( ( StringValue ) expr );
        else {
//            System.err.println("How we got here?");
            return null;
        }
    }


    private Boolean isSubtype(ActorDeclaration child, ActorDeclaration other) {
        boolean result = false;
        ActorDeclaration father = other, childDummy = child;
        do{
            try {
                SymbolTableActorItem fatherActor = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + childDummy.getParentName());
                father = fatherActor.getActorDeclaration();
                if (father == null) break;
                Identifier childId = childDummy.getName();
                Identifier fatherId = father.getName();
                if(Objects.equals(fatherId.getName(), childId.getName())) {
                    result = true;
                    break;
                }
                childDummy = father;
            } catch (ItemNotFoundException ignored) {
                break;
            }
        }while(father != null);
        return result;
    }
    // Brand new isSubtype function 😎
    private Boolean isSubtype(String child, String other) {
        boolean result = false;
        String childDummy = child;
        while (true) {
            try {
                SymbolTableActorItem childItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + childDummy);
                SymbolTableActorItem fatherItem = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + other);
                ActorDeclaration childActor = childItem.getActorDeclaration();
                ActorDeclaration fatherActor = fatherItem.getActorDeclaration();
                Identifier realFatherId = childActor.getParentName();
                if(realFatherId == null) break;
                Identifier fatherId = fatherActor.getName();
                if (realFatherId.getName().equals(fatherId.getName())) {
                    result = true;
                    break;
                }
                childDummy = fatherId.getName();
            } catch (ItemNotFoundException ignored) {
                break;
            }
        }
        return result;
    }


    @Override
    public void visit(Program program) {
        for(ActorDeclaration actorDeclaration : program.getActors())
            this.visit(actorDeclaration);
        mainFlag = true;
        this.visit(program.getMain());
    }

    @Override
    public void visit(ActorDeclaration actorDeclaration) {
        try {
            curr_act = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + actorDeclaration.getName().getName());
        } catch (ItemNotFoundException ignored) {}
        Identifier fatherId = actorDeclaration.getParentName();
        if (fatherId != null) {
            try {
                SymbolTable.root.get(SymbolTableActorItem.STARTKEY + fatherId.getName());
            } catch (ItemNotFoundException ignored){
                System.out.println("Line:" + actorDeclaration.getLine() + ":actor " + fatherId.getName() + " is not declared");
            }
        }
        for(VarDeclaration varDeclaration: actorDeclaration.getKnownActors())
            this.visit(varDeclaration);
//        for(VarDeclaration varDeclaration: actorDeclaration.getActorVars())
//            this.visit(varDeclaration);
        if(actorDeclaration.getInitHandler() != null) {
            initFlag = true;
            this.visit(actorDeclaration.getInitHandler());
            initFlag = false;
        }
        for(MsgHandlerDeclaration msgHandlerDeclaration: actorDeclaration.getMsgHandlers())
            this.visit(msgHandlerDeclaration);
    }

    @Override
    public void visit(HandlerDeclaration handlerDeclaration) {
        try {
            currHandler = (SymbolTableHandlerItem) curr_act.getActorSymbolTable().get(SymbolTableHandlerItem.STARTKEY+handlerDeclaration.getName().getName());
        } catch (ItemNotFoundException ignored) {}

        for(Statement bodyStmnt: handlerDeclaration.getBody())
            visitStatement(bodyStmnt);
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        if(varDeclaration.getType() instanceof ActorType) {
            Type varDecId = varDeclaration.getType();
            try {
                SymbolTable.root.get(SymbolTableActorItem.STARTKEY + varDecId.toString());
            } catch (ItemNotFoundException ignored) {
                System.out.println("Line:" + varDeclaration.getLine() + ":actor " + varDecId.toString() + " is not declared");
            }
        }

    }

    @Override
    public void visit(Main mainActors) {
        // DONE: visit actor identifiers
        // Nothing More :)
        for (ActorInstantiation actorInstantiation : mainActors.getMainActors()) {
            visit(actorInstantiation);
        }
    }

    @Override
    public void visit(ActorInstantiation actorInstantiation) {
        Identifier actorId = actorInstantiation.getIdentifier();
        SymbolTableMainItem mainItem = null;
        Type actorType = null;
        ActorDeclaration actor = null;
        try {
            mainItem = (SymbolTableMainItem) SymbolTable.root.get(SymbolTableMainItem.STARTKEY + "main");
            actorType = actorInstantiation.getType();
            SymbolTableActorItem actorSymbolTable = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + actorType.toString());
            actor = actorSymbolTable.getActorDeclaration();
        } catch (ItemNotFoundException ignored) {
            System.out.println("Line:" + actorInstantiation.getLine() + ":actor " + actorId.getName() + " is not declared");
            return;
        }
        ArrayList<VarDeclaration> knownActors = actor.getKnownActors();
        ArrayList<Identifier> passedKnownActors = actorInstantiation.getKnownActors();
        if(knownActors == null)
            knownActors = new ArrayList<>();
        if(passedKnownActors == null)
            passedKnownActors = new ArrayList<>();

        if (passedKnownActors.size() != knownActors.size()){
            for(Identifier knownActId : passedKnownActors){
                try {
                    mainItem.getMainSymbolTable().get(SymbolTableVariableItem.STARTKEY + knownActId.getName());
                } catch (ItemNotFoundException ignored) {
                    System.out.println("Line:"  + actorInstantiation.getLine() + ":variable " + knownActId.getName() + " is not declared");
//                return;
                }
            }
            System.out.println("Line:" + actorInstantiation.getLine() + ":knownactors do not match with definition");
//            System.err.println("I'm Mr. Meeseeks! Existence is pain! 5");
            for (Expression arg : actorInstantiation.getInitArgs())
                visitExpr(arg);
//            System.err.println("I'm Mr. Meeseeks! Existence is pain! 6");
            return;
        }

        for (int i = 0; i < passedKnownActors.size(); i++) {
            String passedKnownActorName = passedKnownActors.get(i).getName();
            SymbolTableVariableItem passedKnownSymTabVar = null;
            try {
                passedKnownSymTabVar = (SymbolTableVariableItem) mainItem.getMainSymbolTable().get(SymbolTableVariableItem.STARTKEY + passedKnownActorName);
            } catch (ItemNotFoundException ignored) {
                System.out.println("Line:"  + actorInstantiation.getLine() + ":variable " + passedKnownActorName + " is not declared");
                continue;
            }
            Type passedKnownActorType = passedKnownSymTabVar.getType();
            Type expectedKnownActorType = knownActors.get(i).getType();
//            System.err.println(i);
//            System.err.println("passedKnownActorType instanceof NoType: " + (passedKnownActorType instanceof NoType));
//            System.err.println("passedKnownActorType.toString().equals(expectedKnownActorType.toString()): " + (passedKnownActorType.toString().equals(expectedKnownActorType.toString())));
//            System.err.println("isSubtype(expectedKnownActorType.toString(), passedKnownActorType.toString()): " + (isSubtype(passedKnownActorType.toString(), expectedKnownActorType.toString())));
            if ( !( passedKnownActorType instanceof NoType || passedKnownActorType.toString().equals(expectedKnownActorType.toString()) ||
                    isSubtype(passedKnownActorType.toString(), expectedKnownActorType.toString()) ) ) {

                for (; i < passedKnownActors.size(); i++) {
                    try {
                        mainItem.getMainSymbolTable().get(SymbolTableVariableItem.STARTKEY + passedKnownActors.get(i).getName());
                    } catch (ItemNotFoundException ignored) {
                        System.out.println("Line:"  + actorInstantiation.getLine() + ":variable " + passedKnownActors.get(i).getName() + " is not declared");
                    }
                }

                System.out.println("Line:" + actorInstantiation.getLine() + ":knownactors do not match with definition");
                for (Expression arg : actorInstantiation.getInitArgs())
                    visitExpr(arg);

                return;
            }
        }

        ArrayList<Expression> passedArgs = actorInstantiation.getInitArgs();
        passedArgs = (passedArgs == null) ? new ArrayList<>() : passedArgs;
        InitHandlerDeclaration initHandlerDeclaration = actor.getInitHandler();
        ArrayList<VarDeclaration> initArgs = (initHandlerDeclaration == null) ? new ArrayList<>() : initHandlerDeclaration.getArgs();
//        System.err.println("ActorName: " + actor.getName().getName());
        if ( initArgs.size() != passedArgs.size() ) {
            if(passedArgs.size() != 0) {
                for (Expression arg : actorInstantiation.getInitArgs())
                    visitExpr(arg);
            }

            System.out.println("Line:" + actorInstantiation.getLine() + ":arguments do not match with definition");
            return;
        }
        for (int i = 0; i < passedArgs.size(); i++) {
            Type initArgType = visitExpr(passedArgs.get(i));
            Type expectedType = initArgs.get(i).getType();
            if ( ! (initArgType instanceof NoType || initArgType.toString().equals(expectedType.toString())) ) {
                System.out.println("Line:" + actorInstantiation.getLine() +
                        ":can not pass type " + initArgType.toString() + " as type " + expectedType.toString());
//                return;
            }
        }
    }

    @Override
    public Type visit(UnaryExpression unaryExpression) {
        UnaryOperator op = unaryExpression.getUnaryOperator();
        Expression operand = unaryExpression.getOperand();
        Type operandType = visitExpr(operand);
//        System.err.println("operand: " + operand.toString() + " operandType: " + (operandType instanceof ActorType) );
//        System.err.println("op == UnaryOperator.not: " + (op == UnaryOperator.not) + " op.toString().equals( UnaryOperator.not.toString()): " + (op.toString().equals( UnaryOperator.not.toString())) );
//        System.err.println("Condition: " + (!(operand instanceof Identifier || operand instanceof ArrayCall)) );
        if (op == UnaryOperator.not) {
            if(operandType instanceof NoType) {
                return new NoType();
            }
            else if(operandType instanceof BooleanType) {
                return new BooleanType();
            }
            else {
                System.out.println("Line:" + unaryExpression.getLine() + ":unsupported operand type for " + op.toString());
                return new NoType();
            }
        } else if (op == UnaryOperator.minus && !(operandType instanceof NoType || operandType instanceof IntType)) {
            System.out.println("Line:" + unaryExpression.getLine() + ":unsupported operand type for " + op.toString());
            return new NoType();
        } else if (operandType instanceof NoType){
//            System.err.println("I'm Mr. Meeseeks! Existence is pain! 4");
            return new NoType();
        }
        Type res = null;
        if( !(operandType instanceof IntType) ){
            System.out.println("Line:" + unaryExpression.getLine() + ":unsupported operand type for " + op.toString());
            res = new NoType();
        }
        if (!(operand instanceof Identifier || operand instanceof ArrayCall || operand instanceof ActorVarAccess)) {
//            System.err.println("I'm Mr. Meeseeks! Existence is pain! 3");
            String error;
            if(op == UnaryOperator.predec || op == UnaryOperator.postdec) {
                error = "lvalue required as decrement operand";
            }else {
                error = "lvalue required as increment operand";
            }
            System.out.println("Line:" + unaryExpression.getLine() + ":" + error);
            res = new NoType();
        }
        if(res == null){
            res = new IntType();
        }
//        System.err.println("I'm Mr. Meeseeks! Existence is pain! 2");
        return res;
    }

    @Override
    public Type visit(BinaryExpression binaryExpression) {
        BinaryOperator binaryOp = binaryExpression.getBinaryOperator();
        Type rvalType = visitExpr(binaryExpression.getRight());
        Type lvalType = visitExpr(binaryExpression.getLeft());
        if (rvalType instanceof NoType && lvalType instanceof NoType) {
//            System.err.println("I'm Mr. Meeseeks! Existence is pain! 1");
            return new NoType();
        }
//        if(!lvalType.equals(rvalType)) {
//            System.out.println("‫‪Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
//            return new NoType();
//        }
        Type res = null;
        switch (binaryOp) {
            //DONE: actors subtypes eq and neq
            case eq: case neq:
                if(rvalType.toString().equals(lvalType.toString())) {
                    res = new BooleanType();
                }
                else if (rvalType instanceof NoType || lvalType instanceof NoType) {
                    res = new NoType();
                }
                else {
                    System.out.println("Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                    res = new NoType();
                }
                break;

            case and: case or:
                if(lvalType instanceof BooleanType && rvalType instanceof BooleanType){
                    res = new BooleanType();
                }
                else if((lvalType instanceof NoType && rvalType instanceof BooleanType) || (rvalType instanceof NoType && lvalType instanceof BooleanType)){
                    res = new NoType();
                }
                else {
                    System.out.println("‫‪Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                    res = new NoType();
                }
                break;


            case gt:case lt:
                if(lvalType instanceof IntType && rvalType instanceof IntType){
                    res = new BooleanType();
                }
                else if((lvalType instanceof NoType && rvalType instanceof IntType) || (rvalType instanceof NoType && lvalType instanceof IntType)){
                    res = new NoType();
                }
                else {
                    System.out.println("‫‪Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                    res = new NoType();
                }
                break;

            case add: case div: case mod: case sub: case mult:
                if(lvalType instanceof IntType && rvalType instanceof IntType){
                    res = new IntType();
                }
                else if((lvalType instanceof NoType && rvalType instanceof IntType) || (rvalType instanceof NoType && lvalType instanceof IntType)){
                    res = new NoType();
                }
                else {
                    System.out.println("‫‪Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                    res = new NoType();
                }
                break;
            //DONE: assign subtypes check
            case assign:
                Expression lval = binaryExpression.getLeft();
                if (rvalType instanceof ActorType && lvalType instanceof ActorType) {
                    ActorType rvalActType = (ActorType) rvalType;
                    ActorType lvalActType = (ActorType) lvalType;
//                    ActorDeclaration rvalActor = rvalActType.getActorDeclaration();
//                    ActorDeclaration lvalActor = lvalActType.getActorDeclaration();
                    if (! isSubtype( rvalActType.getName().getName(), lvalActType.getName().getName() ) ) {
                        System.out.println("Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                        res = new NoType();
                    } else res = lvalActType;
                }
                if(!(lval instanceof Identifier || lval instanceof ArrayCall)) {
                    System.out.println("Line:" + binaryExpression.getLine() + ":left‬‬ ‫‪side‬‬ ‫‪of‬‬ ‫‪assignment‬‬ ‫‪must‬‬ ‫‪be‬‬ ‫‪a‬‬ ‫‪valid‬‬ ‫‪lvalue");
                    res = new NoType();
                }
                if ( ! rvalType.toString().equals(lvalType.toString())  ) {
                    System.out.println("Line:" + binaryExpression.getLine() + ":unsupported operand type for " + binaryExpression.getBinaryOperator().toString());
                    res = new NoType();
                }
                res = (res == null) ?  lvalType : res;
                break;
        }
        return res;
    }

    @Override
    public Type visit(ArrayCall arrayCall) {
        //DONE: visit Identifier
        Type arrIndexType = visitExpr(arrayCall.getIndex());
        if(!checkType(arrIndexType, Primitives.INT)){
            System.out.println("‫‪Line:" + arrayCall.getLine() + ":array index ‫‪must‬‬ ‫‪be‬‬ an Integer");
            return new NoType();
        }
        Identifier arrInstance = (Identifier) arrayCall.getArrayInstance();
        Type arrInstType = visitExpr(arrInstance);
        if (!(arrInstType instanceof ArrayType || arrInstType instanceof NoType)) {
            System.out.println("‫‪Line:" + arrayCall.getLine() + ":identifier should be an array");
            return new NoType();
        }
        return new IntType();
    }

    @Override
    public Type visit(ActorVarAccess actorVarAccess) {
        Type selfTypeOrNoType = visitExpr(actorVarAccess.getSelf());
        if (selfTypeOrNoType instanceof NoType) return new NoType();
//        Identifier selfActorId = ( (ActorType) selfTypeOrNoType ).getName();
        SymbolTableVariableItem actorSymbol;
        try {
            actorSymbol = (SymbolTableVariableItem) curr_act.getActorSymbolTable().get(SymbolTableVariableItem.STARTKEY + actorVarAccess.getVariable().getName());
        } catch (ItemNotFoundException ignored) {
            System.out.println("Line:" + actorVarAccess.getLine() + ":variable " + actorVarAccess.getVariable().getName() + " is not declared");
            return new NoType();
        }
        return actorSymbol.getType();
    }

    @Override
    public Type visit(Identifier identifier) {
        SymbolTableVariableItem idVarItem;
        try {
            idVarItem = (SymbolTableVariableItem) currHandler.getHandlerSymbolTable().get(SymbolTableVariableItem.STARTKEY + identifier.getName());
        } catch (ItemNotFoundException ignored) {
            System.out.println("‫‪Line:" + identifier.getLine() + "‪:‫‪variable " + identifier.getName() + "‬‬ ‫‪is‬‬ ‫‪not‬‬ ‫‪declared‬‬");
            return new NoType();
        }
        return idVarItem.getType();
    }

    @Override
    public Type visit(Self self) {
        if(mainFlag) {
            System.out.println("Line:" + self.getLine() + ":self doesn't refer to any actor");
            return new NoType();
        }
        ActorDeclaration selfActor = curr_act.getActorDeclaration();
        Identifier selfName = selfActor.getName();
        return new ActorType(selfName);
    }

    @Override
    public Type visit(Sender sender) {
        if(initFlag){
            System.out.println("‫‪Line:" + sender.getLine() + "‪:‫‪no sender in initial msghandler‫‬‬");
            return new NoType();
        }
        return new ActorType(new Identifier("sender"));
    }

    @Override
    public Type visit(BooleanValue value) {
        return new BooleanType();
    }

    @Override
    public Type visit(IntValue value) {
        return new IntType();
    }

    @Override
    public Type visit(StringValue value) {
        return new StringType();
    }

    @Override
    public void visit(Block block) {
        for (Statement blockStmnt : block.getStatements()){
            visitStatement(blockStmnt);
        }
    }

    @Override
    public void visit(Conditional conditional) {
        Type ifCondType = visitExpr(conditional.getExpression());
        if(!checkType(ifCondType, Primitives.BOOLEAN)){
            System.out.println("‫‪Line:" + conditional.getLine() + ":condition‬‬ ‫‪type‬‬ ‫‪must‬‬ ‫‪be‬‬ boolean");
        }
        visitStatement(conditional.getThenBody());
        visitStatement(conditional.getElseBody());
    }

    @Override
    public void visit(For loop) {
        forFlag++;
        visitStatement(loop.getInitialize());
        Type loopCondType = visitExpr(loop.getCondition());
        if(!checkType(loopCondType, Primitives.BOOLEAN)){
            System.out.println("‫‪Line:" + loop.getLine() + ":condition‬‬ ‫‪type‬‬ ‫‪must‬‬ ‫‪be‬‬ boolean");
        }
        visitStatement(loop.getUpdate());
        visitStatement(loop.getBody());
        forFlag--;
    }

    @Override
    public void visit(Break breakLoop) {
        if(forFlag <= 0){
            System.out.println("Line:" + breakLoop.getLine() + ":break statement not within loop");
        }
    }

    @Override
    public void visit(Continue continueLoop) {
        if(forFlag <= 0){
            System.out.println("Line:" + continueLoop.getLine() + ":continue statement not within loop");
        }
    }

    @Override
    public void visit(MsgHandlerCall msgHandlerCall) {
        Expression instanceId = msgHandlerCall.getInstance();
        Type instType = visitExpr(instanceId);
        if (!(instType instanceof NoType || instType instanceof ActorType)) {
            System.out.println("Line:" + msgHandlerCall.getLine() + ":variable " + ((Identifier) msgHandlerCall.getInstance()).getName() + " is not callable");
            for (Expression arg : msgHandlerCall.getArgs()) {
                visitExpr(arg);
            }
            return;
        }
        if( instType instanceof NoType || ( (ActorType) instType).getName().getName().equals("sender")){
            for (Expression arg : msgHandlerCall.getArgs()) {
                visitExpr(arg);
            }
            return;
        }
        ActorType instanceType = (ActorType) instType;
        SymbolTableActorItem actorInstance = null;
        try {
            actorInstance = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + instanceType.getName().getName());
        } catch (ItemNotFoundException ignored) {
            System.out.println("Line:" + msgHandlerCall.getLine() + ":variable " + instanceType.getName().getName() + " is not callable");
            for (Expression arg : msgHandlerCall.getArgs()) {
                visitExpr(arg);
            }
            return;
        }

        SymbolTableHandlerItem handlerItem = null;
        HandlerDeclaration expectedHandler = null;
//        SymbolTableActorItem tableActorItem = null;

        try {
            handlerItem = (SymbolTableHandlerItem) actorInstance.getActorSymbolTable().get(SymbolTableHandlerItem.STARTKEY + msgHandlerCall.getMsgHandlerName().getName());
            expectedHandler = handlerItem.getHandlerDeclaration();
        } catch (ItemNotFoundException ignored) {
            System.out.println("Line:" + msgHandlerCall.getLine() + ":there is no msghandler named " + msgHandlerCall.getMsgHandlerName().getName() + " in actor " + actorInstance.getActorDeclaration().getName().getName() );
            for (Expression arg : msgHandlerCall.getArgs()) {
                visitExpr(arg);
            }
            return;
        }

        ActorDeclaration actorInstanceDeclaration = actorInstance.getActorDeclaration();
//        ArrayList<MsgHandlerDeclaration> handlers = actorInstanceDeclaration.getMsgHandlers();
//        for (MsgHandlerDeclaration msgHandler : handlers) {
//            if (msgHandler.getName().getName().equals(msgHandlerCall.getMsgHandlerName().getName())) {
//                expectedHandler = msgHandler;
//                break;
//            }
//        }

        ArrayList<VarDeclaration> arguments = expectedHandler.getArgs();
        ArrayList<Expression> passedArguments = msgHandlerCall.getArgs();
        if(arguments.size() != passedArguments.size()) {
            System.out.println("Line:" + msgHandlerCall.getLine() + ":arguments do not match with definition");
            for (Expression arg : msgHandlerCall.getArgs()) {
                visitExpr(arg);
            }
            return;
        }

        for (int i = 0; i < arguments.size(); i++) {
            Type passedArgType = visitExpr(passedArguments.get(i));
            if (passedArgType instanceof NoType) continue;
            VarDeclaration declaredArg = arguments.get(i);
            if (declaredArg.getType() != passedArgType) {
                System.out.println("Line:" + msgHandlerCall.getLine() + ":arguments do not match with definition");
            }
        }

//        ActorDeclaration currActor = curr_act.getActorDeclaration();
//        Identifier currActorIdent = currActor.getName();
//        String currActorName = currActorIdent.getName();
//
//        if(instType.toString().equals(currActorName)) {
//            handlers = currActor.getMsgHandlers();
//        }
//        else {
//            inst = (Identifier) msgHandlerCall.getInstance();
//            ArrayList<VarDeclaration> knownActors = currActor.getKnownActors();
//            boolean foundKnownActor = false;
//            VarDeclaration instAct = null;
//            for (VarDeclaration knownAct : knownActors) {
//                Identifier knownActId = knownAct.getIdentifier();
//                if (knownActId.getName().equals(inst.getName())) {
//                    instAct = knownAct;
//                    foundKnownActor = true;
//                    break;
//                }
//            }
//            if (!foundKnownActor) {
//                System.out.println("Line:" + msgHandlerCall.getLine() + ":variable " + inst.getName() + " is not callable");
//                for (Expression arg : msgHandlerCall.getArgs()) {
//                    visitExpr(arg);
//                }
//                return;
//            } else {
//                SymbolTableActorItem actorInst = null;
//                try {
//                    ActorType knownActType = (ActorType) instAct.getType();
//                    actorInst = (SymbolTableActorItem) SymbolTable.root.get(SymbolTableActorItem.STARTKEY + knownActType.getName().getName());
//                    ActorDeclaration actorDeclaration = actorInst.getActorDeclaration();
//                    handlers = actorDeclaration.getMsgHandlers();
//                } catch (ItemNotFoundException ignored) {
//                    for (Expression arg : msgHandlerCall.getArgs()) {
//                        visitExpr(arg);
//                    }
//                    return;
//                }
//            }
//        }
//
//
//
//        MsgHandlerDeclaration expectedMsg = null;
//        String msgHandlerName = msgHandlerCall.getMsgHandlerName().getName();
//        for (MsgHandlerDeclaration msg : handlers) {
//            if(msg.getName().getName().equals(msgHandlerName)) {
//                expectedMsg = msg;
//                break;
//            }
//        }
//        if(expectedMsg == null) {
////            System.out.println("Line:" + msgHandlerCall.getLine() + ":there is no msghandler name " + msg.);
//        }
    }

    @Override
    public void visit(Print print) {
        Type printArgType = print.getArg().getType();
        if (!(printArgType instanceof IntType || printArgType instanceof StringType || printArgType instanceof BooleanType
            || printArgType instanceof ArrayType || printArgType instanceof NoType)) {
            System.out.println("Line:" + print.getLine() +":unsupported type for print");
        }
    }

    @Override
    //TODO: check assign or assignment???
    public void visit(Assign assign) {
        Expression lval = assign.getlValue();
        Expression rval = assign.getrValue();
        Type lvalType = visitExpr(lval);
        Type rvalType = visitExpr(rval);

        if (rvalType instanceof ActorType && lvalType instanceof ActorType) {
            ActorType rvalActType = (ActorType) rvalType;
            ActorType lvalActType = (ActorType) lvalType;
//            ActorDeclaration rvalActor = rvalActType.getActorDeclaration();
//            ActorDeclaration lvalActor = lvalActType.getActorDeclaration();
            if (! isSubtype(rvalActType.getName().getName(), lvalActType.getName().getName()) ) {
                System.out.println("Line:" + assign.getLine() + ":unsupported operand type for assignment");
            }
            return;
        }

        if (lvalType instanceof ArrayType && rvalType instanceof ArrayType) {
            if( ((ArrayType)lvalType).getSize() != ((ArrayType)rvalType).getSize() ) {
                System.out.println("Line:" + assign.getLine() + ":operation assign requires equal array sizes");
                return;
            }
        }

        if (!(lval instanceof Identifier || lval instanceof ArrayCall)){
            System.out.println("Line:" + assign.getLine() + ":left‬‬ ‫‪side‬‬ ‫‪of‬‬ ‫‪assignment‬‬ ‫‪must‬‬ ‫‪be‬‬ ‫‪a‬‬ ‫‪valid‬‬ ‫‪lvalue");
            return;
        }
        if (rvalType instanceof NoType || lvalType instanceof NoType){
            return;
        }
        if(!lvalType.toString().equals(rvalType.toString())) {
            System.out.println("Line:" + assign.getLine() + ":unsupported‬‬ ‫‪operand‬‬ ‫‪type‬‬ ‫‪for assignment‬‬");
        }
    }
}
