package main.visitor;

import main.ast.node.Main;
import main.ast.node.Program;
import main.ast.node.declaration.ActorDeclaration;
import main.ast.node.declaration.ActorInstantiation;
import main.ast.node.declaration.VarDeclaration;
import main.ast.node.declaration.handler.HandlerDeclaration;
import main.ast.node.expression.*;
import main.ast.node.expression.values.BooleanValue;
import main.ast.node.expression.values.IntValue;
import main.ast.node.expression.values.StringValue;
import main.ast.node.statement.*;
import main.ast.type.Type;

public interface InterfaceTypeChecker {

    void visit (Program program);

    //Declarations
    void visit (ActorDeclaration actorDeclaration);
    void visit (HandlerDeclaration handlerDeclaration);
    void visit (VarDeclaration varDeclaration);

    //main
    void visit(Main mainActors);
    void visit(ActorInstantiation actorInstantiation);

    //Expressions
    Type visit(UnaryExpression unaryExpression);
    Type visit(BinaryExpression binaryExpression);
    Type visit(ArrayCall arrayCall);
    Type visit(ActorVarAccess actorVarAccess);
    Type visit(Identifier identifier);
    Type visit(Self self);
    Type visit(Sender sender);
    Type visit(BooleanValue value);
    Type visit(IntValue value);
    Type visit(StringValue value);

    //Statements
    void visit(Block block);
    void visit(Conditional conditional);
    void visit(For loop);
    void visit(Break breakLoop);
    void visit(Continue continueLoop);
    void visit(MsgHandlerCall msgHandlerCall);
    void visit(Print print);
    void visit(Assign assign);
}
